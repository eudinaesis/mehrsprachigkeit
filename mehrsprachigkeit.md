---
title: "Support for multilingualism in Austrian upper-secondary schools: a neglected challenge"
author: Peter Northup
date: 12.1.2021
lang: en
width: 1920
height: 1200
bibliography: /Users/pnorthup/Documents/bibs/mehr.json
nocite: |
	@herzog-punzenbergerAustriaEquityResearch2019, @steinerFruherBildungsabbruchNeue2016, @fischerErfolgreichLernenUnd2019, [@herzog-punzenbergerMultilingualEducationLight2017], [@herzog-punzenbergerSuccessfulIntegrationMigrant2016], [@herzog-punzenbergerSituationMehrsprachigerSchuler2012], [@crulSchoolCareersSecondgeneration2012]
---

# The focus on multilingualism in Austrian schools has been focused on lower-secondary education and earlier

## And rightly so!

Crul et al. -@crulSchoolCareersSecondgeneration2012 showed that the low academic performance of Turkish 2nd-generation immigrants in Austria (and Germany) was driven by *early sorting* combined with difficulty managing the SS1 → Lehre/SS2 transition

> " The place occupied by German and Austrian Turks at its low end is largely determined by the late start in school and the early selection – a situation that requires a lot of practical support from parents, though which many are not able to give." - [@crulSchoolCareersSecondgeneration2012, 138]

# But Austrian SS2 has serious difficulties supporting multilingualism, too!

## Drop-out rate quite high across SS2 …

![From @oberwimmerNationalerBildungsberichtOesterreich2019, Figure C7.c](media/ausbildungsverlauf-neueinsteiger.png){.stretch}

## … but much worse for multilingual students

![From @oberwimmerNationalerBildungsberichtOesterreich2019, Figure C7.f](media/10th-grade-by-language.png){.stretch}

# Why is the pipeline so leaky?

## Sorting at earlier stages means success in SS2 is structurally seen as the students' & parents' responsibility

Repeating/dropping out seen as normal

- "If they can't follow the lessons, they shouldn't be here" – Easier to encourage children to change schools than change how we teach

## Curriculum structure: "Zwei-Klasse Mehrsprachigkeit"

> "Die österreichische Bildungs- und Sprachunterrichtspolitik erklärt programmatisch, Mehrsprachigkeit zu fördern. Das konkrete Sprachenregime jedoch reduziert diesen Ansatz auf die Förderung einer Elitemehrsprachigkeit, die sich weitgehend am traditionellen Sprachenkanon der Gymnasien orientiert und auf die Veränderungen in der Gesellschaft nur mit großer Verzögerung reagiert." – Hans-Jürgen Krumm -@Krumm2013ZweiKlasse

- The most common "migrant languages" are *very* rarely offered as required "second foreign language"
- Many SS2 tracks are oriented towards the "classic" 19th-C curriculum
    - Latin as a requirement!
    - … many university degrees require Latin, too!

## Assessment

- Almost exclusively *evaluative* (not *formative*) assessment: assessent for *testing qualifications* instead of *supporting learning*
- In addition, assessment happens mostly through tests (Schularbeiten) rather than papers, projects, etc.
- Connection to qualifications via tests → emphasis on *procedural* fairness: "objective" evaluation
- Individualization of evaluation standards hard to square with this paradigm

# Conclusion

## What is to be done?

- Structure: legal right to SS2
- Curriculum: *any* language should count as the required "second modern foreign language"
- Teaching forms
    - Project work and "flipped classroom" strategies make it easier for students who have trouble understanding or are slower reading German
    - "Content and Language Integrated Learning" (CLIL) strategies also applicable to teaching in German!
- Assessment
    - Focus on formative instead of evaluative assessment
    - "Competence-oriented assessment" – clearly distinguish between *subject* and *language* (and especially: *German-language*) competencies

## Thank you for your time! Questions?

## Literature